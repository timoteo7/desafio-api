## Clone

```
git clone https://bitbucket.org/timoteo7/desafio-api.git
```

## Install

```sh
composer install
```

## Usage

```sh
php artisan serve
```

## Setup (Optional)

Se não quiser usar o Servidor web embutido do PHP e já tiver um servidor Apache (http://localhost/desafio-api/)

```
sudo chmod -R 777 storage/framework storage/logs
```

## ToDo
- [x] ~~Documentar com MarkDown~~
- [x] ~~Diagrama do collection de exemplo~~
- [ ] AccountController
- [ ] Factory


## Routes
| Method    | URI                   | Action                    |
|-----------|-----------------------|---------------------------|
| POST      | api/account           | AuthController@store      |
| POST      | api/login             | AuthController@login      |
| POST      | api/account/deposit   | AccountController@update  |
| GET/HEAD  | api/account/balance   | AccountController@show    |


## History

```
composer create-project laravel/lumen desafio-api && cd desafio-api
git init && git add . && git commit -m 'Lumen'
wget https://raw.githubusercontent.com/timoteo7/files/master/.htaccess
wget https://raw.githubusercontent.com/Scalingo/sample-php-lumen/master/server.php
```

```
composer require laracasts/generators --dev
sed -i "/AppServiceProvider/ s/\/\/ //" bootstrap/app.php
sed -i "N;/{\n\s*\/\//a \\\t\tif (\$this->app->environment() == 'local') {\n\t\t\t\$this->app->register('Laracasts\\\Generators\\\GeneratorsServiceProvider');\n\t\t}" app/Providers/AppServiceProvider.php
                 s="name:";            s+="string(191), ";\
                s+="email:";           s+="string(150):unique, ";\
                s+="password:";        s+="string(191):nullable, ";\
                s+="balance:";         s+="unsignedFloat:nullable ";\
php artisan make:migration:schema create_accounts_table --schema="$s" --model=0
php artisan migrate
```

```
composer require irazasyed/larasupport
composer require reliese/laravel --dev
mkdir config 2>/dev/null
cp vendor/reliese/laravel/config/models.php config/models.php
sed -i "s/app_path('Models')/base_path('app\/Models')/" config/models.php
sed -i "/app->configure('app');/a \\\$app->configure('models');" bootstrap/app.php
sed -i "N;/{\n\s*\/\//a \\\t\tif (\$this->app->environment() == 'local') {\n\t\t\t\$this->app->register(\\\Reliese\\\Coders\\\CodersServiceProvider::class);\n\t\t}" app/Providers/AppServiceProvider.php
php artisan code:models
```

```
composer require tymon/jwt-auth
sed -i "/AuthServiceProvider/ s/\/\/ //" bootstrap/app.php
sed -i "/EventServiceProvider/a \\\$app->register(Tymon\\\JWTAuth\\\Providers\\\LumenServiceProvider::class);" bootstrap/app.php
sed -i "1N;$!N; s/\/\/ \(\$app->routeMiddleware.*\n\)\/\/ \(\s*'auth'.*\n\)\/\/ /\1\2/;P;D" bootstrap/app.php
php artisan jwt:secret
sed -i "/app->withFacades();/ s/\/\/ //" bootstrap/app.php
sed -i "/app->withEloquent();/ s/\/\/ //" bootstrap/app.php
wget -P config https://gist.githubusercontent.com/mabasic/7979d67ce3ec75a5938e3d14575736a6/raw/61d1e5d49a450c3aae2289ef4c55c900e99180b6/auth.php
wget -P app/Http/Controllers https://raw.githubusercontent.com/buzdyk/Lumen/master/app/Http/Controllers/AuthController.php
sed -i "N;/router->app->version();\n});/a \\\n\$router->group(['prefix' => 'api'], function () use (\$router) {\n\t\$router->post('account', 'AuthController@register');\n\t\$router->post('login', 'AuthController@login');\n});" routes/web.php
sed -i "s/'users'/'accounts'/" config/auth.php
sed -i "s/User::/Models\\\Account::/" config/auth.php
```

```
php artisan make:controller AccountController -mModels/Account --api
```

## Diagram
![Diagram](resources/diagram.png "Diagram" )

## Topics (tags)
##### #Account  #Crypto

## Author

👤 **[Timóteo](https://timoteo7.github.io/)**



## Built With

* [Lumen](https://github.com/laravel/lumen) - The stunningly fast micro-framework by Laravel.
* [Laravel-Generators-Extended](https://github.com/laracasts/Laravel-5-Generators-Extended) - Extends the core file generators.
* [Reliese Laravel](https://github.com/reliese/laravel) - Collection of Components for code generation.
* [JWT-Auth](https://github.com/tymondesigns/jwt-auth) - JSON Web Token Authentication.


## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
